import Gramatica
import ManejaArchivos
import ManejaGramatica
import re

grammar1 = """
% Comentario 1
% Comentario 2
#markers δεζηθικλμνξο
#vars xyz
(Etiqueta_1) manzana -> apple (Etiqueta_2)
(Etiqueta_2) papitas -> bag and chips (Etiqueta_1)
(Etiqueta_3) tienda -> .shop 
"""

#κ -> Λthe

grammar2 = """
% Hola soy un comentario muy lindo
% A este comentario le vale madres el saprisa
#symbols abc
#symbols defx
#markers δεζηθικλμνξο
#vars x
(Etiqueta_1) ab -> x 
(Etiqueta_2) x -> .bb 
"""

grammar3 = """
% COMENTARIO 1
#symbols daos
#vars x
#markers β
(P1) βx -> xβ (P1)
(P2) xβ -> Λ.
(P3) x -> βx (P1)
"""

grammar4 = """
"A" -> apple
"B" -> bag
"C" -> shop
"D" -> .the
"""

if __name__ == '__main__':
        gramatica = Gramatica.Gramatica()
        manejaGramatica = ManejaGramatica.ManejaGramatica()
        #No es necesario validar que existan, ya que si el resultado de la lista es vacio, el valor por defecto no se cambia
        #if manejaGramatica.compilamarcadores(grammar3):
        gramatica.setMarcadores(manejaGramatica.compilamarcadores(grammar4))

        #if manejaGramatica.compilacomentarios(grammar3):
        gramatica.setComentarios(manejaGramatica.compilacomentarios(grammar4))
        
        #if manejaGramatica.compilasimbolos(grammar3):
        gramatica.setSimbolos(manejaGramatica.compilasimbolos(grammar4))

        #if manejaGramatica.compilavariables(grammar3):
        gramatica.setVariables(manejaGramatica.compilavariables(grammar4))

        manejaGramatica.creaRgxReglas(gramatica)
        gramatica.setReglas(manejaGramatica.creaListaDeElementosRegla(grammar4, gramatica.getVariables(), gramatica.getSimbolos()))

        #hilera = 'I bought an δ ε from η ζ'
        #hilera = 'dadosa'
        #hilera = manejaGramatica.reemplazaDos(hilera, gramatica)
        #print(hilera)

        

        #PRINT
        '''print("COMENTARIOS= " + gramatica.getComentarios())
        print("SIMBOLOS= " + gramatica.getSimbolos())
        print("MARCADORES= " + gramatica.getMarcadores())
        print("VARIABLES= " + gramatica.getVariables())
        #Si se desean ver los elementos de la regla:
        print("REGLAS= ")
        for regla in gramatica.getReglas():
            print('Regla:')
            print(regla.getEtiqueta())
            print(regla.getPatron())
            print(regla.getReemplazo())
            print(regla.getInstruccion())
            print(regla.getTerminal())
            print('\n')
        #print(manejaGramatica.reemplazaDos("abcd", gramatica.getReglas()))'''
        #Ejecucion de la gramatica 3:
        #hilera = 'I bought an δ ε from η ζ'
        hilera = 'A B C D'
        terminal = None
        contador = 0
        tamano = len(gramatica.getReglas())
        while True:
            regla = gramatica.getReglas()[contador]            
            encuentra = re.search(regla.getPatron(), hilera)
            print(encuentra)
            if encuentra:
                terminal = regla.getTerminal()
                char = encuentra.group(0)
                print(char)
                reemplazo = manejaGramatica.limpioReemplazo(regla.getReemplazo(), char, gramatica.getVariables(), gramatica.getMarcadores())
                print(reemplazo)
                hilera = re.sub(regla.getPatron(), reemplazo, hilera, 1)
                print(hilera)
                print(regla.getEtiqueta())
                print(regla.getInstruccion())
                if terminal:
                    break
                contador = gramatica.getReglas().index(gramatica.demeReglaConEtiqueta(regla.getInstruccion()))
            else:
                if contador < tamano:
                    contador += 1
                else:
                    contador = 0
