import xml.etree.ElementTree as ET
import Gramatica
import ManejaGramatica


class ManejaArchivos:
    def __init__(self):
        pass

    def escribeXML(self, texto, nombreArchivo):
        grammar = Gramatica.Gramatica()
        manejaGramatica = ManejaGramatica.ManejaGramatica()
        grammar.setComentarios(manejaGramatica.obtienecomentarios(texto))
        grammar.setMarcadores(manejaGramatica.compilamarcadores(texto))
        grammar.setSimbolos(manejaGramatica.compilasimbolos(texto))
        grammar.setVariables(manejaGramatica.compilavariables(texto))
        manejaGramatica.creaRgxReglas(grammar)
        grammar.setReglas(
            manejaGramatica.creaListaDeElementosReglaParaGuardar(texto))
        raiz = ET.Element("gramatica")

        comentarios = ET.SubElement(raiz, "comentarios")
        self.ingresaComentariosDeListaXML(grammar.getComentarios(), comentarios)

        simbolos = ET.SubElement(raiz, "simbolos")
        simbolos.text = grammar.getSimbolos()

        marcadores = ET.SubElement(raiz, "marcadores")
        marcadores.text = grammar.getMarcadores()

        variables = ET.SubElement(raiz, "variables")
        variables.text = grammar.getVariables()

        reglas = ET.SubElement(raiz, "reglas")
        self.ingresaReglasDeListaXML(grammar.getReglas(), reglas)

        arbol = ET.ElementTree(raiz)
        arbol.write(nombreArchivo, encoding="UTF-8")

    def ingresaComentariosDeListaXML(self, lista, subRaiz):
        for elemento in lista:
            comentario = ET.SubElement(subRaiz, "comentario")
            comentario.text = elemento

    def ingresaComentariosDeListaTXT(self, lista):
        comentarios = ''
        for elemento in lista:
            comentarios += str('%' + elemento) + '\n'
        return comentarios

    def ingresaReglasDeListaXML(self, lista, subRaiz):
        for regla in lista:
            reglaNodo = ET.SubElement(subRaiz, "regla")
            etiqueta = ET.SubElement(reglaNodo, "etiqueta")
            etiqueta.text = ''.join(regla.getEtiqueta())

            patron = ET.SubElement(reglaNodo, "patron")
            patron.text = ''.join(regla.getPatron())

            reemplazo = ET.SubElement(reglaNodo, "reemplazo")
            reemplazo.text = ''.join(regla.getReemplazo())

            instruccion = ET.SubElement(reglaNodo, "instruccion")
            instruccion.text = ''.join(regla.getInstruccion())

            terminal = ET.SubElement(reglaNodo, "terminal")
            if regla.getTerminal():
                terminal.text = ''.join(regla.getTerminal())

    def escribeTXT(self, texto, nombreArchivo):
        grammar = Gramatica.Gramatica()
        manejaGramatica = ManejaGramatica.ManejaGramatica()
        grammar.setComentarios(manejaGramatica.obtienecomentarios(texto))
        grammar.setMarcadores(manejaGramatica.compilamarcadores(texto))
        grammar.setSimbolos(manejaGramatica.compilasimbolos(texto))
        grammar.setVariables(manejaGramatica.compilavariables(texto))
        manejaGramatica.creaRgxReglas(grammar)
        grammar.setReglas(manejaGramatica.creaListaDeElementosReglaParaGuardar(
            texto))
        file = open(nombreArchivo, "w", encoding="utf-8")
        file.write(self.ingresaComentariosDeListaTXT(grammar.getComentarios()))
        file.write("#symbols " + grammar.getSimbolos() + "\n")
        file.write("#markers " + grammar.getMarcadores() + "\n")
        file.write("#vars " + grammar.getVariables() + "\n")
        for regla in grammar.getReglas():
            file.write(str(regla.getEtiqueta()))
            file.write(regla.getPatron()+' -> ')
            if regla.getTerminal():
                file.write(str(regla.getTerminal()))
            file.write(str(regla.getReemplazo())+' ')
            file.write(str(regla.getInstruccion())+"\n")

        file.close()

    def abreXML(self, pathGrammar):  
        grammar = ''      
        tree = ET.parse(pathGrammar)
        root = tree.getroot()
        for comentarios in root.findall('comentarios'):
            for comentario in comentarios.findall('comentario'):
                grammar = grammar + '% ' + comentario.text + '\n'

        for simbolos in root.iter('simbolos'):
            grammar = grammar + '#symbols ' + simbolos.text + '\n'

        for marcadores in root.iter('marcadores'):
           grammar = grammar + '#markers ' + marcadores.text + '\n'

        for variables in root.iter('variables'):
            grammar = grammar + '#vars ' + variables.text + '\n'

        for reglas in root.findall('reglas'):
            for regla in reglas.findall('regla'):
                rg = str(self.obtengoValorDeXML(regla, 'etiqueta')) + ' '
                rg += self.obtengoValorDeXML(regla, 'patron')+ ' -> '
                term = self.obtengoValorDeXML(regla, 'terminal')
                if term:
                    rg += term
                rg += self.obtengoValorDeXML(regla, 'reemplazo')
                rg += ' ' + self.obtengoValorDeXML(regla, 'instruccion')
                grammar += rg + '\n'
        return grammar.strip()

    def obtengoValorDeXML(self, nodo, nombreNodo):
        val = nodo.find(nombreNodo).text
        if val:
            return val
        else:
            return ''