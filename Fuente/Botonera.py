from tkinter import ttk
from tkinter import Button
from tkinter import Tk
import tkinter as tk

class Paleta(ttk.Frame):
    def imprimeCaracter(self, caracter):
        self.vis_prin.entrada_hilera.insert("end", caracter)


    def __init__(self, main_window, vista_principal):
        super().__init__(main_window)
        self.vis_prin = vista_principal
        main_window.title("Greek alphabet")
        alfabetogriego = 'αβγδεζηθικλμνξοπρστυφχψω'
        fuente = 'Helvetica 10 bold'
        flecha = '->'
        anchoAlto = 2
        columna = 0
        fila = 0
        contador = 0
        while (contador <= 23):
                self.botonTemp = Button(self, text = alfabetogriego[contador], height = anchoAlto, width = anchoAlto, font = fuente, command = lambda letra=alfabetogriego[contador]: self.imprimeCaracter(letra))
                self.botonTemp.grid(row = fila, column = columna)
                columna += 1
                contador += 1
                if columna >= 5:
                        columna = 0
                        fila +=1
        self.botonTemp = Button(self, text = '->', height = anchoAlto, width = anchoAlto, font = fuente, command = lambda letra=flecha: self.imprimeCaracter(letra) )
        self.botonTemp.grid(row = fila, column = columna)
        self.botonTemp = Button(self, text = 'Λ', height = anchoAlto, width = anchoAlto, font = fuente, command = lambda letra="Λ": self.imprimeCaracter(letra) )
        self.botonTemp.grid(row = fila+1, column = 0)
        main_window.configure(width=135, height=270)
        self.place(width=600, height=300)
        main_window.mainloop()



